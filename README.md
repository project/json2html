# JSON2HTML

The JSON2HTML module displays JSON as a HTML table.

Provides a field type that can be added to entities.

Multi level tables are collapsed and can be expanded 
to navigate through the data.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/json2html).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/json2html).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Add a JSON2HTML field to a content type, user, or other entity.


## Usage

- JSON2HTML field can get JSON data from two kinds of source:

   - URL
   - File

- If using a JSON file first add it as a Media item and then use it's 
  ID in the Media ID field.
  
- To display specific parts of the JSON data set the Path field.
  `https://jsontostring.com/jsonpath/`


## Maintainers

Current maintainers:
- Robert Castelo - [robertcastelo](https://www.drupal.org/u/robert-castelo)
