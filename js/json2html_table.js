(function ($) {
  Drupal.behaviors.json2html = {
    attach: function (context, settings) {
      $.each(settings.json2html, function (key, item) {
        var containerJS = document.getElementById('DynamicGrid-' + key);
        var jsonGrid = new JSONGrid(preprocessData(item.data[0]), containerJS);
        var htmlTable = jsonGrid.render();
        $(containerJS, context).once('add-div').append(htmlTable).fadeIn();
        $('span.string:contains(undefined)').replaceWith($('<span></span>'));
      });

      // Turns arrays that aren't handled properly by library to strings
      function preprocessData(unfilteredData) {
        const processedData = JSON.stringify(unfilteredData).replace(
          /\["(.*?)"\]/g,
          function (x) {
            // If list has multiple elements, turn into comma separated string
            if (x.split('"').length > 3) {
              return `"${x
                .slice(1, -1)
                .replace(/\"/g, '')
                .replace(/\,/g, ', ')}"`;
              // Else if list has just 1 element, no commas are needed
            } else {
              return x.slice(1, -1);
            }
          }
        );
        return JSON.parse(processedData);
      }
    },
  };
})(jQuery);
