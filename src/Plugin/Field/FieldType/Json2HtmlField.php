<?php

namespace Drupal\json2html\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of JSON2HTML.
 *
 * @FieldType(
 *   id = "json2html",
 *   label = @Translation("JSON2HTML"),
 *   default_formatter = "json2html",
 *   default_widget = "json2html",
 * )
 */
class Json2HtmlField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'url' => [
          'description' => 'URL to JSON data.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'path' => [
          'description' => 'Path to data.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'target_id' => [
          'description' => 'ID of Media entity.',
          'type' => 'int',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['url'] = DataDefinition::create('uri')->setLabel(t('URL'));
    $properties['path'] = DataDefinition::create('string')->setLabel(t('Path'));
    $properties['target_id'] = DataDefinition::create('integer')->setLabel(t('Media ID'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $empty = TRUE;
    $url = $this->get('url')->getValue();
    $target_id = $this->get('target_id')->getValue();

    // Workaround Drupal core bug not accepting empty value in compound field.
    if ($url == 'http://example.com') {
      $url = '';
    }

    if (!empty($url) || !empty($target_id)) {
      $empty = FALSE;
    }

    return $empty;
  }

}
