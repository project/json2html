<?php

namespace Drupal\json2html\Plugin\Field\FieldFormatter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Component\Serialization\Json;
use JsonPath\JsonObject;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Plugin implementation of the 'JSON2HTML' formatter.
 *
 * @FieldFormatter(
 *   id = "json2html",
 *   label = @Translation("JSON2HTML"),
 *   field_types = {
 *     "json2html"
 *   }
 * )
 */
class Json2HtmlFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a EntityReferenceEntityFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      if (!method_exists($item, 'get')) {
        return;
      }

      $target_id = $item->get('target_id')->getValue();
      $url = $item->get('url')->getValue();

      // Workaround Drupal core bug not accepting empty value in compound field.
      if ($url == 'http://example.com') {
        $url = '';
      }

      if ($target_id) {

        /** @var \Drupal\media\Entity\Media $media_entity */
        $media_entity = $this->entityTypeManager->getStorage('media')->load($target_id);

        if ($media_entity) {
          $file_id = $media_entity->getSource()->getSourceFieldValue($media_entity);
          $file = $this->entityTypeManager->getStorage('file')->load($file_id);
          $uri = $file->getFileUri();
        }
        else {
          $this->messenger->addError($this->t('Media not found'));
          return;
        }

      }
      elseif ($url) {
        $uri = $url;
      }
      else {
        // Output nothing if there is no URL or file set.
        return;
      }

      // Set Path to root object or array if no path is set.
      $path = $item->get('path')->getValue() ? $item->get('path')->getValue() : '$';

      // Get string from URI and convert to a JSON resource.
      $source_string = file_get_contents($uri);

      if (empty($source_string)) {
        $this->messenger->addWarning($this->t('No JSON data found at URL.'));
        return $element;
      }

      $source_json = Json::decode($source_string);

      if (empty($source_json)) {
        $this->messenger->addError($this->t('JSON data is not valid.'));
        return $element;
      }

      $json = new JsonObject($source_json);
      $data = $json->get($path);

      // Container div that the table HTML will be appended to.
      $element[$delta] = ['#markup' => '<div id="DynamicGrid-' . $delta . '"></div><div class="clearfix"></div>'];

      // CSS and Javascript to be added to the page when this field is shown.
      $element[$delta]['#attached']['library'][] = 'json2html/json2html';

      // Data and settings for the HTML table to be generated.
      $element[$delta]['#attached']['drupalSettings']['json2html'][$delta]['data'] = $data;
    }

    return $element;
  }

}
