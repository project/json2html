<?php

namespace Drupal\json2html\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A widget bar.
 *
 * @FieldWidget(
 *   id = "json2html",
 *   label = @Translation("JSON2HTML widget"),
 *   field_types = {
 *     "json2html"
 *   }
 * )
 */
class Json2HtmlWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $item = $items[$delta];

    // Placeholder URL added to get around Drupal core bug.
    $element['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => empty($item->url) ||  $item->url == 'http://example.com' ? '' : $item->url,
      '#maxlength' => 2048,
    ];

    $element['target_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Media ID'),
      '#description' => $this->t('To use a file as the JSON source instead of a URL set its Media entity ID.'),
      '#default_value' => $item->target_id ?? NULL,
    ];

    $element['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $item->path ?? '',
      '#maxlength' => 255,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);

    // Drupal core will throw an error if no value is set for a compound field.
    foreach ($values as $delta => $value) {
      if (!isset($value['target_id']) || empty($value['target_id'])) {
        $values[$delta]['target_id'] = 0;
      }

      if (!isset($value['url']) || empty($value['url'])) {
        $values[$delta]['url'] = 'http://example.com';
      }
    }

    return $values;
  }

}
